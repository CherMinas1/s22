//buyer details


let buyersDetailsJSON =
{
	buyerId: ObjectId,
	firstName: String,
	lastName: String,
	userName: String,
	email: string,
	password: string,
	mobileNumber: string,
	address: String,
	isUser: false

}

let UsersDetailsJSON =
{
	sellerId: ObjectId,
	firstName: String,
	lastName: String,
	userName: String,
	email: string,
	password: string,
	mobileNumber: string,
	isUser: true,
	storeName: String,
	storeAddress: String
}

let productsJSON =

{
	 productName: String,
    productDescription: String,
    productPrice: String,
    productStocks: String,
    IsActive?
    productSKU: String

}

let orderedProductsJSON =
{
	orderId: String,
	productId: String,
	quantitiy: Number,
	price: Number,
	subTotal: Number
}

let orderJSON =
{
	userId: String,
	transactionDate: String,
	status: String,
	total: Number
}

let deliveryJSON =
{
	userId: String,
	shippingOption: String,
	paymentOption: String,
	orderedItems: String
	totalPayment: String
	deliveryAddress: String,
}

let paymentJSON =
{
	storeName: String,
	paymentOption: String,
	orderedItems: string,
	totalPayment: string,
	storeName: String,
	buyerId: String

}

